dropdb -h localhost -U astra flowerbed
createdb -h localhost -U astra flowerbed

python manage.py loaddata fixtures/common/common.json
python manage.py loaddata fixtures/common/directories.json
python manage.py loaddata fixtures/common/language.json
python manage.py loaddata fixtures/common/ownership.json
python manage.py loaddata fixtures/common/security_classification.json
python manage.py loaddata fixtures/common/grnti.json


python manage.py auto_create_entities

python manage.py populate_report_constructor

celery worker -A flowerbed -B -l info -S django
